// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "WeaponBase.h"
PRAGMA_DISABLE_OPTIMIZATION
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWeaponBase() {}
// Cross Module References
	FPS_API UFunction* Z_Construct_UFunction_AWeaponBase_PrimaryWeaponFire();
	FPS_API UClass* Z_Construct_UClass_AWeaponBase();
	FPS_API UFunction* Z_Construct_UFunction_AWeaponBase_SecondaryWeaponFire();
	FPS_API UClass* Z_Construct_UClass_AWeaponBase_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_FPS();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
	FPS_API UClass* Z_Construct_UClass_UFireWeaponInt_NoRegister();
// End Cross Module References
	static FName NAME_AWeaponBase_PrimaryWeaponFire = FName(TEXT("PrimaryWeaponFire"));
	bool AWeaponBase::PrimaryWeaponFire()
	{
		WeaponBase_eventPrimaryWeaponFire_Parms Parms;
		ProcessEvent(FindFunctionChecked(NAME_AWeaponBase_PrimaryWeaponFire),&Parms);
		return !!Parms.ReturnValue;
	}
	static FName NAME_AWeaponBase_SecondaryWeaponFire = FName(TEXT("SecondaryWeaponFire"));
	bool AWeaponBase::SecondaryWeaponFire()
	{
		WeaponBase_eventSecondaryWeaponFire_Parms Parms;
		ProcessEvent(FindFunctionChecked(NAME_AWeaponBase_SecondaryWeaponFire),&Parms);
		return !!Parms.ReturnValue;
	}
	void AWeaponBase::StaticRegisterNativesAWeaponBase()
	{
		UClass* Class = AWeaponBase::StaticClass();
		static const TNameNativePtrPair<ANSICHAR> AnsiFuncs[] = {
			{ "PrimaryWeaponFire", (Native)&AWeaponBase::execPrimaryWeaponFire },
			{ "SecondaryWeaponFire", (Native)&AWeaponBase::execSecondaryWeaponFire },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, AnsiFuncs, ARRAY_COUNT(AnsiFuncs));
	}
	UFunction* Z_Construct_UFunction_AWeaponBase_PrimaryWeaponFire()
	{
		UObject* Outer = Z_Construct_UClass_AWeaponBase();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("PrimaryWeaponFire"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x0C020C00, 65535, sizeof(WeaponBase_eventPrimaryWeaponFire_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(ReturnValue, WeaponBase_eventPrimaryWeaponFire_Parms);
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(ReturnValue, WeaponBase_eventPrimaryWeaponFire_Parms), 0x0010000000000580, CPP_BOOL_PROPERTY_BITMASK(ReturnValue, WeaponBase_eventPrimaryWeaponFire_Parms), sizeof(bool), true);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Game"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("WeaponBase.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AWeaponBase_SecondaryWeaponFire()
	{
		UObject* Outer = Z_Construct_UClass_AWeaponBase();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("SecondaryWeaponFire"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x0C020C00, 65535, sizeof(WeaponBase_eventSecondaryWeaponFire_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(ReturnValue, WeaponBase_eventSecondaryWeaponFire_Parms);
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(ReturnValue, WeaponBase_eventSecondaryWeaponFire_Parms), 0x0010000000000580, CPP_BOOL_PROPERTY_BITMASK(ReturnValue, WeaponBase_eventSecondaryWeaponFire_Parms), sizeof(bool), true);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Game"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("WeaponBase.h"));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AWeaponBase_NoRegister()
	{
		return AWeaponBase::StaticClass();
	}
	UClass* Z_Construct_UClass_AWeaponBase()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_AActor();
			Z_Construct_UPackage__Script_FPS();
			OuterClass = AWeaponBase::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= (EClassFlags)0x20900080u;

				OuterClass->LinkChild(Z_Construct_UFunction_AWeaponBase_PrimaryWeaponFire());
				OuterClass->LinkChild(Z_Construct_UFunction_AWeaponBase_SecondaryWeaponFire());

				UProperty* NewProp_MeshComponent = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("MeshComponent"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(MeshComponent, AWeaponBase), 0x004000000008000d, Z_Construct_UClass_UStaticMeshComponent_NoRegister());
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_AWeaponBase_PrimaryWeaponFire(), "PrimaryWeaponFire"); // 3655795216
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_AWeaponBase_SecondaryWeaponFire(), "SecondaryWeaponFire"); // 1790171175
				static TCppClassTypeInfo<TCppClassTypeTraits<AWeaponBase> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->Interfaces.Add(FImplementedInterface(Z_Construct_UClass_UFireWeaponInt_NoRegister(), VTABLE_OFFSET(AWeaponBase, IFireWeaponInt), false ));
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("WeaponBase.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("WeaponBase.h"));
				MetaData->SetValue(NewProp_MeshComponent, TEXT("AllowPrivateAccess"), TEXT("true"));
				MetaData->SetValue(NewProp_MeshComponent, TEXT("Category"), TEXT("Game"));
				MetaData->SetValue(NewProp_MeshComponent, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_MeshComponent, TEXT("ModuleRelativePath"), TEXT("WeaponBase.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(AWeaponBase, 2290159868);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AWeaponBase(Z_Construct_UClass_AWeaponBase, &AWeaponBase::StaticClass, TEXT("/Script/FPS"), TEXT("AWeaponBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AWeaponBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
PRAGMA_ENABLE_OPTIMIZATION
