// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "PlayerCharacter.h"
PRAGMA_DISABLE_OPTIMIZATION
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePlayerCharacter() {}
// Cross Module References
	FPS_API UClass* Z_Construct_UClass_APlayerCharacter_NoRegister();
	FPS_API UClass* Z_Construct_UClass_APlayerCharacter();
	ENGINE_API UClass* Z_Construct_UClass_ACharacter();
	UPackage* Z_Construct_UPackage__Script_FPS();
	FPS_API UClass* Z_Construct_UClass_AWeaponBase_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UCameraComponent_NoRegister();
// End Cross Module References
	void APlayerCharacter::StaticRegisterNativesAPlayerCharacter()
	{
	}
	UClass* Z_Construct_UClass_APlayerCharacter_NoRegister()
	{
		return APlayerCharacter::StaticClass();
	}
	UClass* Z_Construct_UClass_APlayerCharacter()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_ACharacter();
			Z_Construct_UPackage__Script_FPS();
			OuterClass = APlayerCharacter::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= (EClassFlags)0x20900080u;


				UProperty* NewProp_SprintingMovementSpeed = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("SprintingMovementSpeed"), RF_Public|RF_Transient|RF_MarkAsNative) UUnsizedIntProperty(CPP_PROPERTY_BASE(SprintingMovementSpeed, APlayerCharacter), 0x0020080000020005);
				UProperty* NewProp_WalkingMovementSpeed = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("WalkingMovementSpeed"), RF_Public|RF_Transient|RF_MarkAsNative) UUnsizedIntProperty(CPP_PROPERTY_BASE(WalkingMovementSpeed, APlayerCharacter), 0x0020080000020005);
				UProperty* NewProp_EquipedWeapon = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("EquipedWeapon"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(EquipedWeapon, APlayerCharacter), 0x0040000000000005, Z_Construct_UClass_AWeaponBase_NoRegister());
				UProperty* NewProp_CameraComponent = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("CameraComponent"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(CameraComponent, APlayerCharacter), 0x00400000000a000d, Z_Construct_UClass_UCameraComponent_NoRegister());
				static TCppClassTypeInfo<TCppClassTypeTraits<APlayerCharacter> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("HideCategories"), TEXT("Navigation"));
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("PlayerCharacter.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("PlayerCharacter.h"));
				MetaData->SetValue(NewProp_SprintingMovementSpeed, TEXT("AllowPrivateAccess"), TEXT("true"));
				MetaData->SetValue(NewProp_SprintingMovementSpeed, TEXT("Category"), TEXT("Game"));
				MetaData->SetValue(NewProp_SprintingMovementSpeed, TEXT("ModuleRelativePath"), TEXT("PlayerCharacter.h"));
				MetaData->SetValue(NewProp_SprintingMovementSpeed, TEXT("ToolTip"), TEXT("Movement speed used when walking"));
				MetaData->SetValue(NewProp_WalkingMovementSpeed, TEXT("AllowPrivateAccess"), TEXT("true"));
				MetaData->SetValue(NewProp_WalkingMovementSpeed, TEXT("Category"), TEXT("Game"));
				MetaData->SetValue(NewProp_WalkingMovementSpeed, TEXT("ModuleRelativePath"), TEXT("PlayerCharacter.h"));
				MetaData->SetValue(NewProp_EquipedWeapon, TEXT("AllowPrivateAccess"), TEXT("true"));
				MetaData->SetValue(NewProp_EquipedWeapon, TEXT("Category"), TEXT("Game"));
				MetaData->SetValue(NewProp_EquipedWeapon, TEXT("ModuleRelativePath"), TEXT("PlayerCharacter.h"));
				MetaData->SetValue(NewProp_CameraComponent, TEXT("AllowPrivateAccess"), TEXT("true"));
				MetaData->SetValue(NewProp_CameraComponent, TEXT("Category"), TEXT("Game"));
				MetaData->SetValue(NewProp_CameraComponent, TEXT("EditInline"), TEXT("true"));
				MetaData->SetValue(NewProp_CameraComponent, TEXT("ModuleRelativePath"), TEXT("PlayerCharacter.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(APlayerCharacter, 1691916261);
	static FCompiledInDefer Z_CompiledInDefer_UClass_APlayerCharacter(Z_Construct_UClass_APlayerCharacter, &APlayerCharacter::StaticClass, TEXT("/Script/FPS"), TEXT("APlayerCharacter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APlayerCharacter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
PRAGMA_ENABLE_OPTIMIZATION
