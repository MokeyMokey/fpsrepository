// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FPS_WeaponBase_generated_h
#error "WeaponBase.generated.h already included, missing '#pragma once' in WeaponBase.h"
#endif
#define FPS_WeaponBase_generated_h

#define FPS_Source_FPS_WeaponBase_h_13_RPC_WRAPPERS \
	virtual bool SecondaryWeaponFire_Implementation(); \
	virtual bool PrimaryWeaponFire_Implementation(); \
 \
	DECLARE_FUNCTION(execSecondaryWeaponFire) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=this->SecondaryWeaponFire_Implementation(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPrimaryWeaponFire) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=this->PrimaryWeaponFire_Implementation(); \
		P_NATIVE_END; \
	}


#define FPS_Source_FPS_WeaponBase_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSecondaryWeaponFire) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=this->SecondaryWeaponFire_Implementation(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPrimaryWeaponFire) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=this->PrimaryWeaponFire_Implementation(); \
		P_NATIVE_END; \
	}


#define FPS_Source_FPS_WeaponBase_h_13_EVENT_PARMS \
	struct WeaponBase_eventPrimaryWeaponFire_Parms \
	{ \
		bool ReturnValue; \
 \
		/** Constructor, initializes return property only **/ \
		WeaponBase_eventPrimaryWeaponFire_Parms() \
			: ReturnValue(false) \
		{ \
		} \
	}; \
	struct WeaponBase_eventSecondaryWeaponFire_Parms \
	{ \
		bool ReturnValue; \
 \
		/** Constructor, initializes return property only **/ \
		WeaponBase_eventSecondaryWeaponFire_Parms() \
			: ReturnValue(false) \
		{ \
		} \
	};


#define FPS_Source_FPS_WeaponBase_h_13_CALLBACK_WRAPPERS
#define FPS_Source_FPS_WeaponBase_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAWeaponBase(); \
	friend FPS_API class UClass* Z_Construct_UClass_AWeaponBase(); \
public: \
	DECLARE_CLASS(AWeaponBase, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/FPS"), NO_API) \
	DECLARE_SERIALIZER(AWeaponBase) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	virtual UObject* _getUObject() const override { return const_cast<AWeaponBase*>(this); }


#define FPS_Source_FPS_WeaponBase_h_13_INCLASS \
private: \
	static void StaticRegisterNativesAWeaponBase(); \
	friend FPS_API class UClass* Z_Construct_UClass_AWeaponBase(); \
public: \
	DECLARE_CLASS(AWeaponBase, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/FPS"), NO_API) \
	DECLARE_SERIALIZER(AWeaponBase) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	virtual UObject* _getUObject() const override { return const_cast<AWeaponBase*>(this); }


#define FPS_Source_FPS_WeaponBase_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AWeaponBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AWeaponBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWeaponBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWeaponBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWeaponBase(AWeaponBase&&); \
	NO_API AWeaponBase(const AWeaponBase&); \
public:


#define FPS_Source_FPS_WeaponBase_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWeaponBase(AWeaponBase&&); \
	NO_API AWeaponBase(const AWeaponBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWeaponBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWeaponBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AWeaponBase)


#define FPS_Source_FPS_WeaponBase_h_13_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__MeshComponent() { return STRUCT_OFFSET(AWeaponBase, MeshComponent); }


#define FPS_Source_FPS_WeaponBase_h_10_PROLOG \
	FPS_Source_FPS_WeaponBase_h_13_EVENT_PARMS


#define FPS_Source_FPS_WeaponBase_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FPS_Source_FPS_WeaponBase_h_13_PRIVATE_PROPERTY_OFFSET \
	FPS_Source_FPS_WeaponBase_h_13_RPC_WRAPPERS \
	FPS_Source_FPS_WeaponBase_h_13_CALLBACK_WRAPPERS \
	FPS_Source_FPS_WeaponBase_h_13_INCLASS \
	FPS_Source_FPS_WeaponBase_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FPS_Source_FPS_WeaponBase_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FPS_Source_FPS_WeaponBase_h_13_PRIVATE_PROPERTY_OFFSET \
	FPS_Source_FPS_WeaponBase_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	FPS_Source_FPS_WeaponBase_h_13_CALLBACK_WRAPPERS \
	FPS_Source_FPS_WeaponBase_h_13_INCLASS_NO_PURE_DECLS \
	FPS_Source_FPS_WeaponBase_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FPS_Source_FPS_WeaponBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
