// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FPS_Pistol_generated_h
#error "Pistol.generated.h already included, missing '#pragma once' in Pistol.h"
#endif
#define FPS_Pistol_generated_h

#define FPS_Source_FPS_Pistol_h_15_RPC_WRAPPERS
#define FPS_Source_FPS_Pistol_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define FPS_Source_FPS_Pistol_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPistol(); \
	friend FPS_API class UClass* Z_Construct_UClass_APistol(); \
public: \
	DECLARE_CLASS(APistol, AWeaponBase, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/FPS"), NO_API) \
	DECLARE_SERIALIZER(APistol) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define FPS_Source_FPS_Pistol_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAPistol(); \
	friend FPS_API class UClass* Z_Construct_UClass_APistol(); \
public: \
	DECLARE_CLASS(APistol, AWeaponBase, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/FPS"), NO_API) \
	DECLARE_SERIALIZER(APistol) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define FPS_Source_FPS_Pistol_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APistol(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APistol) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APistol); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APistol); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APistol(APistol&&); \
	NO_API APistol(const APistol&); \
public:


#define FPS_Source_FPS_Pistol_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APistol() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APistol(APistol&&); \
	NO_API APistol(const APistol&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APistol); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APistol); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APistol)


#define FPS_Source_FPS_Pistol_h_15_PRIVATE_PROPERTY_OFFSET
#define FPS_Source_FPS_Pistol_h_12_PROLOG
#define FPS_Source_FPS_Pistol_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FPS_Source_FPS_Pistol_h_15_PRIVATE_PROPERTY_OFFSET \
	FPS_Source_FPS_Pistol_h_15_RPC_WRAPPERS \
	FPS_Source_FPS_Pistol_h_15_INCLASS \
	FPS_Source_FPS_Pistol_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FPS_Source_FPS_Pistol_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FPS_Source_FPS_Pistol_h_15_PRIVATE_PROPERTY_OFFSET \
	FPS_Source_FPS_Pistol_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	FPS_Source_FPS_Pistol_h_15_INCLASS_NO_PURE_DECLS \
	FPS_Source_FPS_Pistol_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FPS_Source_FPS_Pistol_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
