// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "FireWeaponInt.h"
PRAGMA_DISABLE_OPTIMIZATION
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFireWeaponInt() {}
// Cross Module References
	FPS_API UFunction* Z_Construct_UFunction_UFireWeaponInt_PrimaryWeaponFire();
	FPS_API UClass* Z_Construct_UClass_UFireWeaponInt();
	FPS_API UFunction* Z_Construct_UFunction_UFireWeaponInt_SecondaryWeaponFire();
	FPS_API UClass* Z_Construct_UClass_UFireWeaponInt_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UInterface();
	UPackage* Z_Construct_UPackage__Script_FPS();
// End Cross Module References
	bool IFireWeaponInt::PrimaryWeaponFire()
	{
		check(0 && "Do not directly call Event functions in Interfaces. Call Execute_PrimaryWeaponFire instead.");
		FireWeaponInt_eventPrimaryWeaponFire_Parms Parms;
		return Parms.ReturnValue;
	}
	bool IFireWeaponInt::SecondaryWeaponFire()
	{
		check(0 && "Do not directly call Event functions in Interfaces. Call Execute_SecondaryWeaponFire instead.");
		FireWeaponInt_eventSecondaryWeaponFire_Parms Parms;
		return Parms.ReturnValue;
	}
	void UFireWeaponInt::StaticRegisterNativesUFireWeaponInt()
	{
		UClass* Class = UFireWeaponInt::StaticClass();
		static const TNameNativePtrPair<ANSICHAR> AnsiFuncs[] = {
			{ "PrimaryWeaponFire", (Native)&IFireWeaponInt::execPrimaryWeaponFire },
			{ "SecondaryWeaponFire", (Native)&IFireWeaponInt::execSecondaryWeaponFire },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, AnsiFuncs, ARRAY_COUNT(AnsiFuncs));
	}
	UFunction* Z_Construct_UFunction_UFireWeaponInt_PrimaryWeaponFire()
	{
		UObject* Outer = Z_Construct_UClass_UFireWeaponInt();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("PrimaryWeaponFire"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x0C020C00, 65535, sizeof(FireWeaponInt_eventPrimaryWeaponFire_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(ReturnValue, FireWeaponInt_eventPrimaryWeaponFire_Parms);
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(ReturnValue, FireWeaponInt_eventPrimaryWeaponFire_Parms), 0x0010000000000580, CPP_BOOL_PROPERTY_BITMASK(ReturnValue, FireWeaponInt_eventPrimaryWeaponFire_Parms), sizeof(bool), true);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Game"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("FireWeaponInt.h"));
#endif
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_UFireWeaponInt_SecondaryWeaponFire()
	{
		UObject* Outer = Z_Construct_UClass_UFireWeaponInt();
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("SecondaryWeaponFire"), RF_Public|RF_Transient|RF_MarkAsNative) UFunction(FObjectInitializer(), nullptr, (EFunctionFlags)0x0C020C00, 65535, sizeof(FireWeaponInt_eventSecondaryWeaponFire_Parms));
			CPP_BOOL_PROPERTY_BITMASK_STRUCT(ReturnValue, FireWeaponInt_eventSecondaryWeaponFire_Parms);
			UProperty* NewProp_ReturnValue = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("ReturnValue"), RF_Public|RF_Transient|RF_MarkAsNative) UBoolProperty(FObjectInitializer(), EC_CppProperty, CPP_BOOL_PROPERTY_OFFSET(ReturnValue, FireWeaponInt_eventSecondaryWeaponFire_Parms), 0x0010000000000580, CPP_BOOL_PROPERTY_BITMASK(ReturnValue, FireWeaponInt_eventSecondaryWeaponFire_Parms), sizeof(bool), true);
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Game"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("FireWeaponInt.h"));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UFireWeaponInt_NoRegister()
	{
		return UFireWeaponInt::StaticClass();
	}
	UClass* Z_Construct_UClass_UFireWeaponInt()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_UInterface();
			Z_Construct_UPackage__Script_FPS();
			OuterClass = UFireWeaponInt::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= (EClassFlags)0x20104081u;

				OuterClass->LinkChild(Z_Construct_UFunction_UFireWeaponInt_PrimaryWeaponFire());
				OuterClass->LinkChild(Z_Construct_UFunction_UFireWeaponInt_SecondaryWeaponFire());

				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_UFireWeaponInt_PrimaryWeaponFire(), "PrimaryWeaponFire"); // 722673438
				OuterClass->AddFunctionToFunctionMapWithOverriddenName(Z_Construct_UFunction_UFireWeaponInt_SecondaryWeaponFire(), "SecondaryWeaponFire"); // 533871302
				static TCppClassTypeInfo<TCppClassTypeTraits<IFireWeaponInt> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("BlueprintType"), TEXT("true"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("FireWeaponInt.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFireWeaponInt, 1767542037);
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFireWeaponInt(Z_Construct_UClass_UFireWeaponInt, &UFireWeaponInt::StaticClass, TEXT("/Script/FPS"), TEXT("UFireWeaponInt"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFireWeaponInt);
	static FName NAME_UFireWeaponInt_PrimaryWeaponFire = FName(TEXT("PrimaryWeaponFire"));
	bool IFireWeaponInt::Execute_PrimaryWeaponFire(UObject* O)
	{
		check(O != NULL);
		check(O->GetClass()->ImplementsInterface(UFireWeaponInt::StaticClass()));
		FireWeaponInt_eventPrimaryWeaponFire_Parms Parms;
		UFunction* const Func = O->FindFunction(NAME_UFireWeaponInt_PrimaryWeaponFire);
		if (Func)
		{
			O->ProcessEvent(Func, &Parms);
		}
		else if (auto I = (IFireWeaponInt*)(O->GetNativeInterfaceAddress(UFireWeaponInt::StaticClass())))
		{
			Parms.ReturnValue = I->PrimaryWeaponFire_Implementation();
		}
		return Parms.ReturnValue;
	}
	static FName NAME_UFireWeaponInt_SecondaryWeaponFire = FName(TEXT("SecondaryWeaponFire"));
	bool IFireWeaponInt::Execute_SecondaryWeaponFire(UObject* O)
	{
		check(O != NULL);
		check(O->GetClass()->ImplementsInterface(UFireWeaponInt::StaticClass()));
		FireWeaponInt_eventSecondaryWeaponFire_Parms Parms;
		UFunction* const Func = O->FindFunction(NAME_UFireWeaponInt_SecondaryWeaponFire);
		if (Func)
		{
			O->ProcessEvent(Func, &Parms);
		}
		else if (auto I = (IFireWeaponInt*)(O->GetNativeInterfaceAddress(UFireWeaponInt::StaticClass())))
		{
			Parms.ReturnValue = I->SecondaryWeaponFire_Implementation();
		}
		return Parms.ReturnValue;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
PRAGMA_ENABLE_OPTIMIZATION
