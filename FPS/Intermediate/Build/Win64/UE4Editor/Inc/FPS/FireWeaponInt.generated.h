// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FPS_FireWeaponInt_generated_h
#error "FireWeaponInt.generated.h already included, missing '#pragma once' in FireWeaponInt.h"
#endif
#define FPS_FireWeaponInt_generated_h

#define FPS_Source_FPS_FireWeaponInt_h_12_RPC_WRAPPERS \
	virtual bool SecondaryWeaponFire_Implementation()=0; \
	virtual bool PrimaryWeaponFire_Implementation()=0; \
 \
	DECLARE_FUNCTION(execSecondaryWeaponFire) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=this->SecondaryWeaponFire_Implementation(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPrimaryWeaponFire) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=this->PrimaryWeaponFire_Implementation(); \
		P_NATIVE_END; \
	}


#define FPS_Source_FPS_FireWeaponInt_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	virtual bool SecondaryWeaponFire_Implementation()=0; \
	virtual bool PrimaryWeaponFire_Implementation()=0; \
 \
	DECLARE_FUNCTION(execSecondaryWeaponFire) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=this->SecondaryWeaponFire_Implementation(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPrimaryWeaponFire) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(bool*)Z_Param__Result=this->PrimaryWeaponFire_Implementation(); \
		P_NATIVE_END; \
	}


#define FPS_Source_FPS_FireWeaponInt_h_12_EVENT_PARMS \
	struct FireWeaponInt_eventPrimaryWeaponFire_Parms \
	{ \
		bool ReturnValue; \
 \
		/** Constructor, initializes return property only **/ \
		FireWeaponInt_eventPrimaryWeaponFire_Parms() \
			: ReturnValue(false) \
		{ \
		} \
	}; \
	struct FireWeaponInt_eventSecondaryWeaponFire_Parms \
	{ \
		bool ReturnValue; \
 \
		/** Constructor, initializes return property only **/ \
		FireWeaponInt_eventSecondaryWeaponFire_Parms() \
			: ReturnValue(false) \
		{ \
		} \
	};


#define FPS_Source_FPS_FireWeaponInt_h_12_CALLBACK_WRAPPERS
#define FPS_Source_FPS_FireWeaponInt_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFireWeaponInt(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFireWeaponInt) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFireWeaponInt); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFireWeaponInt); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFireWeaponInt(UFireWeaponInt&&); \
	NO_API UFireWeaponInt(const UFireWeaponInt&); \
public:


#define FPS_Source_FPS_FireWeaponInt_h_12_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFireWeaponInt(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFireWeaponInt(UFireWeaponInt&&); \
	NO_API UFireWeaponInt(const UFireWeaponInt&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFireWeaponInt); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFireWeaponInt); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFireWeaponInt)


#undef GENERATED_UINTERFACE_BODY_COMMON
#define GENERATED_UINTERFACE_BODY_COMMON() \
private: \
	static void StaticRegisterNativesUFireWeaponInt(); \
	friend FPS_API class UClass* Z_Construct_UClass_UFireWeaponInt(); \
public: \
	DECLARE_CLASS(UFireWeaponInt, UInterface, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Interface), 0, TEXT("/Script/FPS"), NO_API) \
	DECLARE_SERIALIZER(UFireWeaponInt) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define FPS_Source_FPS_FireWeaponInt_h_12_GENERATED_BODY_LEGACY \
		PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	GENERATED_UINTERFACE_BODY_COMMON() \
	FPS_Source_FPS_FireWeaponInt_h_12_STANDARD_CONSTRUCTORS \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FPS_Source_FPS_FireWeaponInt_h_12_GENERATED_BODY \
	PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	GENERATED_UINTERFACE_BODY_COMMON() \
	FPS_Source_FPS_FireWeaponInt_h_12_ENHANCED_CONSTRUCTORS \
private: \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FPS_Source_FPS_FireWeaponInt_h_12_INCLASS_IINTERFACE_NO_PURE_DECLS \
protected: \
	virtual ~IFireWeaponInt() {} \
public: \
	typedef UFireWeaponInt UClassType; \
	static bool Execute_PrimaryWeaponFire(UObject* O); \
	static bool Execute_SecondaryWeaponFire(UObject* O); \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define FPS_Source_FPS_FireWeaponInt_h_12_INCLASS_IINTERFACE \
protected: \
	virtual ~IFireWeaponInt() {} \
public: \
	typedef UFireWeaponInt UClassType; \
	static bool Execute_PrimaryWeaponFire(UObject* O); \
	static bool Execute_SecondaryWeaponFire(UObject* O); \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define FPS_Source_FPS_FireWeaponInt_h_9_PROLOG \
	FPS_Source_FPS_FireWeaponInt_h_12_EVENT_PARMS


#define FPS_Source_FPS_FireWeaponInt_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FPS_Source_FPS_FireWeaponInt_h_12_RPC_WRAPPERS \
	FPS_Source_FPS_FireWeaponInt_h_12_CALLBACK_WRAPPERS \
	FPS_Source_FPS_FireWeaponInt_h_12_INCLASS_IINTERFACE \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FPS_Source_FPS_FireWeaponInt_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FPS_Source_FPS_FireWeaponInt_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	FPS_Source_FPS_FireWeaponInt_h_12_CALLBACK_WRAPPERS \
	FPS_Source_FPS_FireWeaponInt_h_12_INCLASS_IINTERFACE_NO_PURE_DECLS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FPS_Source_FPS_FireWeaponInt_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
