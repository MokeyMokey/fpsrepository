// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "PlayerCharacter.generated.h"

UCLASS()
class FPS_API APlayerCharacter : public ACharacter
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Game, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* CameraComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Game, meta = (AllowPrivateAccess = "true"))
		class AWeaponBase* EquipedWeapon;

public:
	// Sets default values for this character's properties
	APlayerCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Game, meta = (AllowPrivateAccess = "true"))
		int WalkingMovementSpeed; //Movement speed used when walking

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Game, meta = (AllowPrivateAccess = "true"))
		int SprintingMovementSpeed; //Movement speed used when sprinting

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	//Movement
	void MoveForward(float Value);
	void MoveRight(float Value);
	void StartSprinting();
	void StopSprinting();

	//Weapon
	void SpawnWeapon();
	void PrimaryFire();
	void SecondaryFire();
};
