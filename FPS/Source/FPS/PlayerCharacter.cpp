// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerCharacter.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "WeaponBase.h"

// Sets default values
APlayerCharacter::APlayerCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	AutoPossessPlayer = EAutoReceiveInput::Player0;

	//Variable Default Values
	WalkingMovementSpeed = 5;
	SprintingMovementSpeed = 15;

	//Camera
	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	CameraComponent->SetRelativeLocation(FVector(0.0f, 0.0f, 50.0f));
	CameraComponent->SetupAttachment(GetCapsuleComponent());
	CameraComponent->bUsePawnControlRotation = true;

	EquipedWeapon = CreateDefaultSubobject<AWeaponBase>(TEXT("Weapon"));
}

// Called when the game starts or when spawned
void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();
	SpawnWeapon();
	
}

// Called every frame
void APlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	check(PlayerInputComponent);
	
	//Movement
	PlayerInputComponent->BindAxis("MoveForward", this, &APlayerCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &APlayerCharacter::MoveRight);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);
	PlayerInputComponent->BindAction("Sprint", IE_Pressed, this, &APlayerCharacter::StartSprinting);
	PlayerInputComponent->BindAction("Sprint", IE_Released, this, &APlayerCharacter::StopSprinting);

	//Camera
	PlayerInputComponent->BindAxis("MouseUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("MouseRight", this, &APawn::AddControllerYawInput);
	
	//Weapon
	PlayerInputComponent->BindAction("PrimaryWeaponFire", IE_Pressed, this, &APlayerCharacter::PrimaryFire);
	PlayerInputComponent->BindAction("SecondaryWeaponFire", IE_Pressed, this, &APlayerCharacter::SecondaryFire);
}

void APlayerCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void APlayerCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void APlayerCharacter::StartSprinting()
{
	
}

void APlayerCharacter::StopSprinting()
{

}

void APlayerCharacter::SpawnWeapon()
{
	FActorSpawnParameters SpawnInfo;
	UWorld* const World = GetWorld();
	if (World != NULL)
	{
		EquipedWeapon = World->SpawnActor<AWeaponBase>(FVector().ZeroVector, FRotator().ZeroRotator, SpawnInfo);
		EquipedWeapon->AttachRootComponentTo(CameraComponent);
	}
}

void APlayerCharacter::PrimaryFire()
{
	UE_LOG(LogTemp, Warning, TEXT("Primary Fire!"));
}

void APlayerCharacter::SecondaryFire()
{
	UE_LOG(LogTemp, Warning, TEXT("Secondary Fire!"));
}
