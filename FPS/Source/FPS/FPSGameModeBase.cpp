// Fill out your copyright notice in the Description page of Project Settings.

#include "FPSGameModeBase.h"
#include "PlayerCharacter.h"

AFPSGameModeBase::AFPSGameModeBase(const FObjectInitializer& IO) :Super(IO)
{
	DefaultPawnClass = APlayerCharacter::StaticClass();
}

