// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FireWeaponInt.h"
#include "WeaponBase.generated.h"

UCLASS()
class FPS_API AWeaponBase : public AActor, public IFireWeaponInt
{
	GENERATED_BODY()

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Game, meta = (AllowPrivateAccess = "true"))
		class UStaticMeshComponent* MeshComponent;
public:	
	// Sets default values for this actor's properties
	AWeaponBase();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Game")
		bool PrimaryWeaponFire();
	virtual bool PrimaryWeaponFire_Implementation() override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Game")
		bool SecondaryWeaponFire();
	virtual bool SecondaryWeaponFire_Implementation() override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
