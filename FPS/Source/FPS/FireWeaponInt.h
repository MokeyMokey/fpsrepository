// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "FireWeaponInt.generated.h"

// This class does not need to be modified.
UINTERFACE(BlueprintType)
class FPS_API UFireWeaponInt : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

class FPS_API IFireWeaponInt
{
	GENERATED_IINTERFACE_BODY()

	
public:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Game")
		bool PrimaryWeaponFire();
	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Game")
		bool SecondaryWeaponFire();
};
