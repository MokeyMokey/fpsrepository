// Fill out your copyright notice in the Description page of Project Settings.

#include "WeaponBase.h"
#include "Components/StaticMeshComponent.h"
#include "Engine.h"

// Sets default values
AWeaponBase::AWeaponBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	RootComponent = MeshComponent;
}

// Called when the game starts or when spawned
void AWeaponBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWeaponBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

bool AWeaponBase::PrimaryWeaponFire_Implementation()
{
	UE_LOG(LogTemp, Warning, TEXT("Primary Fire! - INT"));
	return true;
}

bool AWeaponBase::SecondaryWeaponFire_Implementation()
{
	UE_LOG(LogTemp, Warning, TEXT("Secondary Fire! - INT"));
	return true;
}